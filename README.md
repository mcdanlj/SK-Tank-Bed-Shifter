The SK-Tank printer does not quite live up to its advertized 350mm X
and 350 mm Y dimensions, because gantry travel on both of those axes
is limited to about 344mm. Additionally, the nozzle in the default
configuration is not well aligned with the bed. It covers most of
the 350mm bed in X (about 345mm), but in Y it fails to cover the rear
20mm of the bed and extends out substantially in front of the bed.

This is a set of adapters to be able to use more of the X and Y
dimensions, at the expense of a few mm of Z.

The design is done in the
[realthunder branch of FreeCAD](https://github.com/realthunder/FreeCAD_assembly3/releases).

The "hole.stl" and "slot.stl" files are the front two kinematic
mount points. They are designed to clip into the two inner front
holes on the front bed support brackets, with no need for screws.
They should be a secure fit with no play.  If you have any play,
increase the size of the posts.

The "back.stl" piece is bolted to the rear support over the tab
using two M5 countersunk screws, with wide flat washers underneath
the support.

The bed must be grounded, and using the plastic kinematic mounts
breaks that grounding. If no ground wire is connected to the bed,
cover the "back.stl" top surface with aluminum tape before installing
the M5 screws.
